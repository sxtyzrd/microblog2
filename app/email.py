from flask import render_template
from flask_mail import Message
from app import mail, app


# 23.3 发送电子邮件的封装函数
def send_email(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender=sender, recipients=recipients)  # 抽象为一个邮件
    msg.body = text_body
    msg.html = html_body
    mail.send(msg)

# 23 发送密码重置电子邮件函数
def send_password_reset_email(user):
    token = user.get_reset_password_token()
    send_email('[Microblog] Reset Your Password',
               sender=app.config['MAIL_USERNAME'],
               recipients=[user.email],
               text_body=render_template('email/reset_password.txt',
                                         user=user, token=token),
               html_body=render_template('email/reset_password.html',
                                         user=user, token=token))
