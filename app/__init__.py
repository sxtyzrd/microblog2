from flask import Flask#从flask包中导入Flask类
from flask_login import LoginManager
from flask_mail import Mail
from flask_migrate  import  Migrate
from flask_sqlalchemy import SQLAlchemy



from config import Config

app = Flask(__name__)#将Flask类的实例 赋值给名为 app 的变量。这个实例成为app包的成员。
app.config.from_object(Config)

db = SQLAlchemy(app)#数据库对象
migrate = Migrate(app, db)#迁移引擎对象
login = LoginManager(app)  # user login
login.login_view='login'  # 1617.7 设置登录视图
mail = Mail(app)  # 23.1 创建一个Mail类对象

#print('等会谁{哪个包或模块}在使用我：',__name__)

#print(app.config['SECRET_KEY'])

from app import routes, models#从app包中导入模块routes